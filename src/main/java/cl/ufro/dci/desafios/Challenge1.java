package cl.ufro.dci.desafios;

import cl.ufro.dci.model.Registro;
import cl.ufro.dci.utils.CSV;
import cl.ufro.dci.utils.TXT;

import java.util.*;

public class Challenge1 {

    public Challenge1(String name, String rute) {
        CSV csv = new CSV();
        List<Registro> registros = csv.leerCSV(name);

        //Creamos arreglos para los meses del csv
        ArrayList<Registro> en = new ArrayList<>();
        ArrayList<Registro> feb = new ArrayList<>();
        ArrayList<Registro> mar = new ArrayList<>();
        ArrayList<Registro> abr = new ArrayList<>();
        ArrayList<Registro> may = new ArrayList<>();
        ArrayList<Registro> jun = new ArrayList<>();
        ArrayList<Registro> jul = new ArrayList<>();

        //agregamos los registros correspondientes al mes
        for(Registro registro : registros){
            Calendar calendario = Calendar.getInstance(); // obtiene una instancia de la clase Calendar
            calendario.setTime(registro.getDate()); // establece la fecha en el objeto Calendar
            int mes = calendario.get(Calendar.MONTH);

            switch (mes){
                case 0:
                    en.add(registro);
                    break;
                case 1:
                    feb.add(registro);
                    break;
                case 2:
                    mar.add(registro);
                    break;
                case 3:
                    abr.add(registro);
                    break;
                case 4:
                    may.add(registro);
                    break;
                case 5:
                    jun.add(registro);
                    break;
                case 6:
                    jul.add(registro);
                    break;
            }
        }

        //agregamos a un arreglo nuevo el filtro de los meses
        ArrayList<List<String>> desafio1 = new ArrayList<>();
        desafio1.add(filtrar(en, "Enero"));
        desafio1.add(filtrar(feb, "Febrero"));
        desafio1.add(filtrar(mar, "Marzo"));
        desafio1.add(filtrar(abr, "Abril"));
        desafio1.add(filtrar(may, "Mayo"));
        desafio1.add(filtrar(jun, "Junio"));
        desafio1.add(filtrar(jul, "Julio"));

        TXT txt = new TXT();
        txt.escribirD1(desafio1, rute);
    }

    public List<String> filtrar(ArrayList<Registro> mes, String mesName){
        // Creamos el HashMap para almacenar los resultados
        Map<String, Integer> map = new HashMap<>();

        // Recorremos el ArrayList y sumamos los valores de los casos activos correspondientes
        for (Registro objeto : mes) {
            String clave = objeto.getCountry();
            int valor = objeto.getActive();

            if (map.containsKey(clave)) {
                valor += map.get(clave);
            }

            map.put(clave, valor);
        }

        // Creamos un nuevo ArrayList con los resultados del HashMap
        List<String> listaResultados = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            String clave = entry.getKey();
            int valor = entry.getValue();
            for(Registro r : mes){
                if(r.getCountry().equals(clave)){
                    listaResultados.add(mesName + ";" + clave + ";" + valor + ";" + r.getRegion());
                    break;
                }
            }
        }
        return listaResultados;
    }
}
