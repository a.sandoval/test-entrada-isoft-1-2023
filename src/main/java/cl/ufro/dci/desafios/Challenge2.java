package cl.ufro.dci.desafios;

import cl.ufro.dci.model.Registro;
import cl.ufro.dci.utils.CSV;
import cl.ufro.dci.utils.TXT;

import java.util.*;

public class Challenge2 {
    public Challenge2(String name, String rute) {
        CSV csv = new CSV();
        ArrayList<Registro> registros = csv.leerCSV(name);

        // Creamos el HashMap para almacenar los resultados de los casos de muertes mayores
        HashMap<String, Integer> map = new HashMap<>();
        for (Registro registro : registros) {
            if (!map.containsKey(registro.getCountry())) {
                map.put(registro.getCountry(), registro.getDeaths());
            } else {
                int max = Math.max(map.get(registro.getCountry()), registro.getDeaths());
                map.put(registro.getCountry(), max);
            }
        }

        ArrayList<String> desafio2 = new ArrayList<>();

        // Filtramos para obtener los datos del dia en especifico donde se obtiene el mayor casos de muerte
        for (String str : map.keySet()) {
            int max = map.get(str);
            for (Registro r : registros){
                if(r.getDeaths() == max){
                    Calendar calendario = Calendar.getInstance(); // obtiene una instancia de la clase Calendar
                    calendario.setTime(r.getDate()); // establece la fecha en el objeto Calendar
                    int dia = calendario.get(Calendar.DAY_OF_MONTH);
                    int mes = calendario.get(Calendar.MONTH)+1;

                    desafio2.add(dia+";"+mes+";"+r.getCountry()+";"+r.getRegion()+";"+max);
                    break;
                }
            }
        }

        TXT txt = new TXT();
        txt.escribirD2(desafio2, rute);
    }
}
