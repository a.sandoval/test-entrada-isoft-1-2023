package cl.ufro.dci.utils;

import cl.ufro.dci.model.Registro;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class CSV {
    //Codigo para leer el csv
    public ArrayList<Registro> leerCSV(String name){
        try {
            File archivo = new File(name);
            Scanner scanner = new Scanner(archivo);
            ArrayList<Registro> registros = new ArrayList<Registro>();

            scanner.useDelimiter(",");

            scanner.nextLine();

            while (scanner.hasNextLine()) {
                String linea = scanner.nextLine();
                String[] campos = linea.split(",");
                Registro r = new Registro();

                SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
                r.setDate(formato.parse(campos[0]));
                r.setCountry(campos[1]);
                r.setConfirmed(Integer.parseInt(campos[2]));
                r.setDeaths(Integer.parseInt(campos[3]));
                r.setRecovered(Integer.parseInt(campos[4]));
                r.setActive(Integer.parseInt(campos[5]));
                r.setRegion(campos[6]);
                registros.add(r);
            }
            scanner.close();

            return registros;
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado");
            return null;
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
