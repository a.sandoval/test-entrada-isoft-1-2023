package cl.ufro.dci.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class TXT {
    //Creacion de los txt para cada desafios

    public void escribirD1(ArrayList<List<String>> meses, String rute){
        try {
            FileWriter myWriter = new FileWriter(rute+"/resultado1.txt");

            myWriter.write("### CASOS ACTIVOS MENSUALES POR PAÍS ###\n");
            myWriter.write("### INICIO ARCHIVO ###\n");
            for(List<String> mes : meses){
                for (String linea : mes){
                    myWriter.write(linea+"\n");
                }
            }
            myWriter.write("### FIN ARCHIVO ###");
            myWriter.close();
        } catch (IOException e) {
            System.out.println("Ha ocurrido un error al crear el archivo.");
            e.printStackTrace();
        }
    }

    public void escribirD2(ArrayList<String> registros, String rute){
        try {
            FileWriter myWriter = new FileWriter(rute+"/resultado2.txt");

            myWriter.write("### MAYOR CANTIDAD DE MUERTES POR CADA PAÍS ###\n");
            myWriter.write("### INICIO ARCHIVO ###\n");
            for(String registro : registros){
                myWriter.write(registro+"\n");
            }
            myWriter.write("### FIN ARCHIVO ###");
            myWriter.close();
        } catch (IOException e) {
            System.out.println("Ha ocurrido un error al crear el archivo.");
            e.printStackTrace();
        }
    }
}
