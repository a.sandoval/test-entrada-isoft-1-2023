package cl.ufro.dci;

import cl.ufro.dci.desafios.Challenge1;
import cl.ufro.dci.desafios.Challenge2;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {
        // java -jar mi_app.jar datasets resultados

        Challenge1 challenge1 = new Challenge1(args[0]+"/COVID19.csv", args[1]);

        Challenge2 challenge2 = new Challenge2(args[0]+"/COVID19.csv", args[1]);
    }
}
